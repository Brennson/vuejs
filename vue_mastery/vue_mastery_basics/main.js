var app = new Vue({
  el: "#app",
  data: {
    product: "Socks",
    image: "./assets/image-green.jpeg",
    inventory: 100,
    details: ["80% cotton", "20% polyester", "gender-neutral"],
    variants: [
      { variantId: 2234, variantColor: "green", variantImage: "./assets/image-green.jpeg" },
      { variantId: 2235, variantColor: "blue", variantImage: "./assets/image-blue.jpg"}
    ],
    cart: 0
  },
  methods:{
      addToCart(){
          this.cart += 1;
      },
      updateProduct(variantImage){
        this.image = variantImage
      }
  }
});
